# Changelog
All notable changes to this project will be documented in this file.


# v1.2 2021/10/28
### Added
 - Función que trata la disminución de tamaño de la esfera.

### Modificated
 - Movimiento de la esfera.
 - Movimiento de la raqueta.

# v1.1 2021/10/27
### Added
 - Cambio cabeceras y creación de Changelog.
 

