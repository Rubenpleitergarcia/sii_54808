
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include "DatosMemCompartida.h"
#include <sys/mman.h>
#include <iostream>

int main(void)
{
	DatosMemCompartida *datosm;
	void *pvoid;
	int fdmem;
	
	fdmem=open("/tmp/datosComp.txt", O_RDWR);

	//Proyectamos el fichero 
	struct stat st;
	fstat(fdmem,&st);
	pvoid=(mmap(NULL,st.st_size,PROT_READ|PROT_WRITE,MAP_SHARED,fdmem,0));
		
	close(fdmem);
	datosm=(DatosMemCompartida*)pvoid;
	
	//Entramos en el bucle infinito dónde bot decide que hacer
	while (1){		
	
		if(datosm->esfera.centro.y > datosm->raqueta1.y1)
			datosm->accion=1;
		else if (datosm->esfera.centro.y < datosm->raqueta1.y2)
			datosm->accion=-1;
		else datosm->accion=0;
		usleep(25000);
	}
	
}

	
