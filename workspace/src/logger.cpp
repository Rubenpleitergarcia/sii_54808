#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>


int main() 
{
	int fd;
	char ch;
	char* nombre="/tmp/FIFO";


// Borra FIFO si existia previamente
unlink(nombre);
/* crea el FIFO */
if (mkfifo(nombre,0600)<0){
	perror("NO puede crearse el FIFO");
	return 1;
}
/* Abre el FIFO*/
fd=open(nombre,O_RDONLY);
if (fd<0){
	perror("NO puede abrirse el FIFO");
	return 1;
}

while (read(fd,&ch,1)==1) 
	write(1,&ch,1);
	
close(fd);
unlink(nombre);
return 1;
}




